﻿namespace SignalRMVCChat.WebSocket.UsersSeparation
{
    public class GetUsersSeparationFormSocketHandler : BaseGetUsersSeparationFormSocketHandler
    {
        public GetUsersSeparationFormSocketHandler() : base("getUsersSeparationFormCallback")
        {
        }
    }
}